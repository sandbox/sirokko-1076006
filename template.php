<?php
// $Id: template.php,v 1.0 2010/04 23:52:57 fon Exp $

function apadesur_body_class($left, $right) {
  $class = array();

  if ($left != '' && $right != '') {
    $class[] = 'sidebars';
  }
  elseif ($left != '') {
    $class[] = 'sidebar-left';
  }
  elseif ($right != '') {
    $class[] = 'sidebar-right';
  }

  if (arg(0) == 'admin') {
    $class[] = 'admin';
  }

  if ($class) {
    print ' class="' . implode(' ', $class) . '"';
  }
}

function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    $breadcrumb[] = drupal_get_title();
    return '<div class="breadcrumb">'. implode(' » ', $breadcrumb) .'</div>';
  }
}

function apadesur_comment_wrapper($content, $node) {
  if (!$content || $node->type == 'forum') {
    return '<div id="comments">'. $content .'</div>';
  }
  else {
    return '<div id="comments"><h2 class="comments">'. t('Comments') .'</h2>'. $content .'</div>';
  }
}

function apadesur_preprocess_page(&$vars) {
  $vars['tabs2'] = menu_secondary_local_tasks();
}

function phptemplate_menu_local_tasks() {
  return menu_primary_local_tasks();
}

function apadesur_comment_submitted($comment) {
  return t('by <strong>!username</strong> | !datetime',
    array(
      '!username' => theme('username', $comment),
      '!datetime' => format_date($comment->timestamp)
    ));
}

function phptemplate_node_submitted($node) {
  return t('!datetime | by <strong>!username</strong>',
    array(
      '!username' => theme('username', $node),
      '!datetime' => format_date($node->created),
    ));
}

function apadesur_preprocess_block(&$vars, $hook) {
  $block = $vars['block'];
  $classes = array('block');
  $classes[] = 'block-' . $block->module;
  $classes[] = 'region-' . $vars['block_zebra'];
  $classes[] = $vars['zebra'];
  $classes[] = 'region-count-' . $vars['block_id'];
  $classes[] = 'count-' . $vars['id'];
  $vars['edit_links_array'] = array();
  $vars['edit_links'] = '';
  if (user_access('administer blocks')) {
    include_once './' . drupal_get_path('theme', 'apadesur') . '/template.block-editing.inc';
    apadesur_preprocess_block_editing($vars, $hook);
    $classes[] = 'with-block-editing';
  }
  $vars['classes'] = implode(' ', $classes);
}

function apadesur_get_ie_styles() {
  $iecss = '<link type="text/css" rel="stylesheet" media="all" href="'. base_path() . path_to_theme() .'/ie.css" />';
  return $iecss;
}

function apadesur_get_ie6_styles() {
  $iecss = '<link type="text/css" rel="stylesheet" media="all" href="'. base_path() . path_to_theme() .'/ie6.css" />';
  return $iecss;
}

function phptemplate_menu_item($link, $has_children, $menu = '', $in_active_trail = FALSE, $extra_class = NULL) {
  static $zebra = FALSE;
  $zebra = !$zebra;
  $class = ($menu ? 'expanded' : ($has_children ? 'collapsed' : 'leaf'));
  if (!empty($extra_class)) {
    $class .= ' '. $extra_class;
  }
  if ($in_active_trail) {
    $class .= ' active-trail';
  }
  if ($zebra) {
    $class .= ' even';
  }
  else {
    $class .= ' odd';
  }
  return '<li class="'. $class .'">'. $link . $menu ."</li>\n";
}
