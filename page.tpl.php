<?php
// $Id: page.tpl.php,v 1.0 2010/04 23:52:58 fon Exp $
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" >
   <head>
      <title><?php print $head_title ?></title>
      <?php print $head ?>
      <?php print $styles ?>
      <?php print $scripts ?>
      <!--[if IE]>
			<script type="text/javascript">
				var e = ("abbr, article, aside, audio, canvas, datalist, details, figure, footer, header, hgroup, mark, menu, meter, nav, output, progress, section, time, video").split(',');
				for (var i=0; i<e.length; i++) {
					document.createElement(e[i]);
				}
			</script>
		<![endif]-->
      <!--[if lte IE 7]>
         <?php print apadesur_get_ie_styles(); ?>
      <![endif]-->
      <!--If Less Than or Equal (lte) to IE 7-->
      <script type="text/javascript" src="script.js"></script>
      <script type="text/javascript" src="http://apadesur.mobify.me/mobify/redirect.js"></script>
      <script type="text/javascript">
      	try{
      			_mobify("http://apadesur.mobify.me/");
      	}
      	catch(err) {};
      </script>
   </head>
   <body<?php print apadesur_body_class($left, $right); ?>>
   	<div class="wrapper">
   		<!-- BLOQUE DE HACERSE SOCIO -->
   		<nav id="header-menu">
   				<ul>
   					<li><a href="/user/register">Hazte socio</a></li>
   				</ul>
   		</nav>
   		
      	<!-- HEADER PERSONALIZADO -->
      	<header class="apadesur"></header>
      	
      	<!-- AQUÍ SE UBICARÁ MENÚ DESPLEGABLE -->
			<nav id="primary-menu">
	   		<?php if (isset($primary_links)) : ?>
	      		<?php
	      			$menu_name = variable_get('menu_primary_links_source','primary-links');
	      			print menu_tree($menu_name);
	      		?>
           	<?php endif; ?>
      	</nav>
      	
        	<!-- LEFT SIDEBAR -->
      	<?php if ($left): ?>
       	   <section id="sidebar-left" class="sidebar">
              <?php print $left ?>
           </section>
      	<?php endif; ?>
        
			<!-- MAIN: En este bloque definimos el comportamiento del contenido a mostrar en función de dónde nos encontremos -->
      	<section id="main">
       	
	   		<!-- MIGUITAS DE PAN -->
         	<?php print $breadcrumb; ?>
		   	
		   	<!-- PESTAÑAS DE OPCIONES DE ADMINISTRACIÓN -->
           	<?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
           	<?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block"><ul class="tabs primary">'. $tabs .'</ul>'; endif; ?>
           	<?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
           	<?php if ($tabs): print '<span class="clear"></span></div>'; endif; ?>
	   
           	<!-- SI HAY MENSAJES QUE MOSTRAR PARA CONFIGURAR ALGO, LO MOSTRARÁ -->
           	<?php if ($show_messages && $messages): print $messages; endif; ?>
	   
           	<!-- DEFINE DÓNDE TE ENCUENTRAS -->
           	<?php print $help; ?>
        
	   		<!-- CONTENIDO DE LA PÁGINA EN CUESTIÓN -->
           	<?php print $content; ?>
      	</section>
      	
			<!-- RIGHT SIDEBAR -->
      	<?php if ($right): ?>
           <section id="sidebar-right" class="sidebar">
              <?php print $right ?>
           </section>
      	<?php endif; ?>
      	
 			<!-- FOOTER PERSONALIZADO -->
        	<footer class="apadesur"></footer>
      </div>
   </body>
</html>
