<?php
// $Id: maintenance-page.tpl.php,v 1.0 2010/04 23:52:58 fon Exp $
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" 
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" >
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    <!--[if lte IE 7]><?php print apadesur_get_ie_styles(); ?><![endif]--> <!--If Less Than or Equal (lte) to IE 7-->
  </head>
  <body<?php print apadesur_body_class($left, $right); ?>>
    <div class="wrapper">
      <div id="header" class="apadesur<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" >
	<head>
   	<title><?php print $head_title ?></title>
    	<?php print $head ?>
    	<?php print $styles ?>
    	<?php print $scripts ?>
    	<!--[if lte IE 7]><?php print apadesur_get_ie_styles(); ?><![endif]--> <!--If Less Than or Equal (lte) to IE 7-->
  	</head>
  	<body<?php print apadesur_body_class($left, $right); ?>>
   	<div class="wrapper">
      	<!-- HEADER PERSONALIZADO -->
      	<div id="header" class="apadesur"></div>
      	<!-- AQUÍ SE UBICARÁ MENÚ DESPLEGABLE -->
      	<div id="nav">
      		<?php if (!$nav): ?>
					<?php if (isset($primary_links)) : ?>
						<!-- ToDo: Nice Menú -->
          			<div class="primary-links">
							<?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
		    			</div>
          		<?php endif; ?>
        		<?php endif; ?>
      	</div>
      	<!-- LEFT SIDEBAR -->
      	<?php if ($left): ?>
       		<div id="sidebar-left" class="sidebar">
         		<?php print $left ?>
        		</div>
      	<?php endif; ?>
         <!-- MAIN: En este bloque definimos el comportamiento del contenido a mostrar en función de dónde nos encontremos -->
      	<div id="main">
        		<!-- MIGUITAS DE PAN -->
        		<?php print $breadcrumb; ?>
				<!-- PESTAÑAS DE OPCIONES DE ADMINISTRACIÓN -->
        		<?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block"><ul class="tabs primary">'. $tabs .'</ul>'; endif; ?>
        		<?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
        		<?php if ($tabs): print '<span class="clear"></span></div>'; endif; ?>
				<!-- SI HAY MENSAJES QUE MOSTRAR PARA CONFIGURAR ALGO, LO MOSTRARÁ -->
        		<?php if ($show_messages && $messages): print $messages; endif; ?>
				<!-- DEFINE DÓNDE TE ENCUENTRAS -->
        		<?php print $help; ?>
        		<!-- CONTENIDO DE LA PÁGINA EN CUESTIÓN -->
        		<?php print $content ?>
      	</div>
      	<!-- RIGHT SIDEBAR -->
      	<?php if ($right): ?>
        		<div id="sidebar-right" class="sidebar">
          		<?php print $right ?>
        		</div>
      	<?php endif; ?>
      	<!-- FOOTER PERSONALIZADO -->
        	<div id="footer" class="apadesur-footer"></div>
		</div>
  	</body>
</html>